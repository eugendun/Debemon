FROM            debian:jessie
MAINTAINER      Eugen Dundukov <eugendun@gmail.com>

# ssh, git, emacs, curl
RUN     apt-get update && apt-get install -y \
                openssh-server \
                git \
                emacs \
                curl &&\
        rm -rf /var/lib/apt/list/*

RUN     mkdir /var/run/sshd
RUN     echo 'root:changepassword' | chpasswd

RUN     sed 's/PermitRootLogin without-password/PermitRootLogin yes/' -i /etc/ssh/sshd_config &&\
        sed 's@session\s*required\s*pam_loginuid.so@session optional pam_loginuid.so@g' -i /etc/pam.d/sshd

EXPOSE  22
CMD     ["/usr/sbin/sshd", "-D"]

# mono
RUN     apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF

RUN     echo "deb http://download.mono-project.com/repo/debian wheezy main" | tee /etc/apt/sources.list.d/mono-xamarin.list &&\
        echo "deb http://download.mono-project.com/repo/debian wheezy-apache24-compat main" | tee -a /etc/apt/sources.list.d/mono-xamarin.list &&\
        echo "deb http://download.mono-project.com/repo/debian wheezy-libjpeg62-compat main" | tee -a /etc/apt/sources.list.d/mono-xamarin.list &&\

        apt-get update && apt-get install -y \
                mono-devel \
                ca-certificates-mono \
                nuget \
                referenceassemblies-pcl &&\
        rm -rf /var/lib/apt/list/*

# omnisharp-server
RUN     cd ~ &&\
        git clone https://github.com/OmniSharp/omnisharp-server.git &&\
        cd omnisharp-server &&\
        git submodule update --init --recursive &&\
        xbuild     

# custom emacs config which is configured to use omnisharp
RUN     cd ~ &&\
        rm -rf .emacs.d &&\
        git clone https://gitlab.com/eugendun/CustomEmacs.git .emacs.d &&\
        emacs --batch -l ~/.emacs.d/install-packages.el
