# [eugen/debemon docker container](https://hub.docker.com/r/eugendun/debemon/)

# Emacs C# IDE with OmniSharp-Server
The main purpose of this image is to provide a simple and quick way to setup an environment for C# projects. The container is based on debian and includes mono, configured emacs and omnisharp-server to provide code completion and IDE like features in emacs.

# Usage

login: `root`
password: `changepassword`

You can use either the docker run command
```sh
sudo docker run -it -P --name <container_name> eugendun/debemon:0.1 "/bin/bash"
```
or start the docker container in daemon
```sh
sudo docker run -d -P --name <container_name> eugendun/debemon:0.1
```
and connect via ssh over the mapped port
```sh
sudo docker port <container_name> 22
```

Checkout your code and run emacs. Emacs tries to search for a solution and starts
the omnisharp-server such that you can directly working in your code.

See for reference
* [OmniSharp-Server](https://github.com/OmniSharp/omnisharp-server)
* [OmniSharp-Emacs](https://github.com/OmniSharp/omnisharp-emacs)
* [Emacs](https://www.gnu.org/software/emacs/)
    * [Configured Emacs](https://gitlab.com/eugendun/CustomEmacs)
* [Mono on Linux](http://www.mono-project.com/docs/getting-started/install/linux/)